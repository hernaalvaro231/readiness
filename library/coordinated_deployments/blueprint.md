# Coordinated deployments

## Background

Over the past 18 months the Delivery team has been working to solve the problem of how we can reliably deploy to GitLab.com as frequently as possible in addition to maintaining a monthly release schedule for self-managed users.

The resulting release and deployment processes provide a repeatable process for achieving these goals and the progress is clearly displayed on the team's key performance indicator Mean Time To Production.

Alongside this work GitLab continues to scale and our solution contains now-unnecessary complexity. Today GitLab consists of more components meaning our deployment approach should no longer center around Omnibus, instead allowing for independent components to be deployed on their own deployment schedule. Additionally the migration to Kubernetes adds a whole new element, with deployments to Virtual Machines as well as Kubernetes needing to be triggered and tracked. As deployments become more frequent, the ability to quickly recover from failure becomes more vital, and the current deployment complexity makes this a significant challenge.

This blueprint outlines our initial thinking about specific problems to meeting these needs as well as reducing complexity in our deployment approach.

## Current status
At the current time (Nov 2020) we have the ability for multiple deploys to GitLab.com each day. Deployments are supported by Apdex monitoring, removing many of the manual steps previously needed to decide whether a particular version could be deployed.

![current status diagram](https://docs.google.com/drawings/d/e/2PACX-1vSL66nYf1q386-kCMo2s4PIP7YMQBOcVi-As9KD5_-unrfzTrPscBVf-QnOqC6pB0meELz1x8Ystv4y/pub?w=960&h=720)

_[Source](https://docs.google.com/drawings/d/1fotLpVljlAToSl-fQhJmGl1jk5Yzq5dx9EgdD07XUI0/edit), GitLab internal use only_

* The auto-deploy release process starts at the "tag" job which finds passing commits and updates versions on the Distribution Team pipelines
* The "pick" and "prepare" release-tools scheduled pipelines are responsible for creating an auto-deploy branch and picking changes into them
* The `GITALY_SERVER_VERSION` file on `gitlab-org/gitlab` is updated in a [scheduled job](https://ops.gitlab.net/gitlab-org/release/tools/pipeline_schedules) named `components:update`

The current pipeline model depends on multiple scheduled release-tools pipelines that tag Distribution team projects which then trigger the Delivery [Deployer pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/deployer) which deploys up to Production.

### Example run using the current status

For the sake of example, we'll look at a [single GitLab EE commit](https://gitlab.com/gitlab-org/gitlab/commit/2978c49f78d) being deployed to production.

#### release-tools

A scheduled [`auto_deploy:tag` job][tag] in release-tools detects changes to
gitlab-rails or its components and:

1. Commits [updates to Omnibus versions](https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/commit/efdb38639b0ae58f7dfc47c6def74c2be7d91093) on gitlab.com
1. Tags Omnibus at the update commit as `13.7.202012011220+2978c49f78d.efdb38639b0`, which [creates a pipeline][omnibus pipeline] on dev.gitlab.org
1. Tags Deployer `master` as `13.7.202012011220+2978c49f78d.efdb38639b0` on ops.gitlab.net
1. Commits [updates to CNG versions](https://gitlab.com/gitlab-org/security/charts/components/images/-/commit/322b1dcd2a9b51bd6c1fd4d258241edb487983b6) on gitlab.com
1. Tags CNG at the update commit as `13.7.202012011220+2978c49f78d` on gitlab.com, which [creates a pipeline][cng pipeline] on dev.gitlab.org
1. Triggers a [Helm pipeline][] on gitlab.com
   1. The triggered Helm pipeline [contains a job that creates the Helm tag][helm tag] `13.7.202012011220+2978c49f78d`
1. Creates a [Sentry release][] as `2978c49f78d`
1. Uploads [metadata][] as `13.7.202012011220.json` on ops.gitlab.net

#### omnibus-gitlab

The [pipeline on the Omnibus tag][omnibus pipeline] builds the package and,
after uploading to the package server, [triggers a Deployer pipeline][deployer
trigger] on the tag created by release-tools.

[tag]: https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/2484807
[omnibus pipeline]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/178530
[cng pipeline]: https://dev.gitlab.org/gitlab/charts/components/images/-/pipelines/178531
[Helm pipeline]: https://gitlab.com/gitlab-org/security/charts/gitlab/-/pipelines/223676944
[helm tag]: https://gitlab.com/gitlab-org/security/charts/gitlab/-/jobs/883192569
[Sentry release]: https://sentry.gitlab.net/gitlab/gitlabcom/releases/2978c49f78d/
[metadata]: https://ops.gitlab.net/gitlab-org/release/metadata/-/blob/master/releases/13/13.7.202012011220.json
[deployer trigger]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/9118896
[deployer pipeline]: https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/364800

#### deployer

The [deployer pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/364800) will:

1. Deploy to staging and:
   1. Trigger a [staging QA pipeline][qa-gstg]
   1. Trigger a release-tools `track-deployment` pipeline
      * `track-deployment` creates a Sentry deploy
1. Deploy to canary and:
   1. Trigger a [canary QA pipeline][qa-cny]
   1. Trigger a [production QA pipeline][qa-gprd]
   1. Trigger a release-tools `track-deployment` pipeline
      * `track-deployment` creates a Sentry deploy
1. Create a manual job that, when executed, will deploy to production and:
   1. Trigger a production QA pipeline
   1. Trigger a release-tools `track-deployment` pipeline
      * `track-deployment` creates a Sentry deploy

_Note that a Canary deploy triggers both Canary and Production QA pipelines.
This is done to ensure that canary migrations haven't broken something on
production._

[qa-gstg]: https://ops.gitlab.net/gitlab-org/quality/staging/-/pipelines/364864
[qa-cny]: https://ops.gitlab.net/gitlab-org/quality/canary/-/pipelines/364946
[qa-gprd]: https://ops.gitlab.net/gitlab-org/quality/production/-/pipelines/364948

That's a lot of pipelines. Here's a visualization:

```mermaid
graph TD
  classDef ops fill:#53777a, color:white
  classDef dev fill:#542437, color:white
  classDef prd fill:#c02942, color:white

  subgraph environments
    ops:::ops
    dev:::dev
    production:::prd
  end

  rtTag{{auto_deploy:tag}}:::ops --> |tag| obTag[/omnibus-gitlab\]:::prd
  rtTag --> |tag| dTag([deployer]):::ops
  rtTag --> |tag| iTag[/CNG\]:::prd
  rtTag -.-> |trigger| hPipeline[/Helm\]:::prd
  rtTag -.-> sentry>Sentry release]
  rtTag -.-> metadata:::ops

  obTag --> |pipeline| obTagPipeline[/omnibus-gitlab\]:::dev
  obTagPipeline -.-> |trigger| dPipeline([deployer]):::ops

  dPipeline --> dStg([gstg]):::ops
  dStg -.-> |trigger| qaStg[/QA staging/]:::ops
  dStg -.-> |trigger| rtTrackStg{{track-deployment}}:::ops
  rtTrackStg -.-> sentryDeployStg>Sentry deploy]

  dStg --> dCny([gprd-cny]):::ops
  dCny -.-> |trigger| qaCny[/QA canary/]:::ops
  dCny -.-> |trigger| qaPrd1[/QA production/]:::ops
  dCny -.-> |trigger| rtTrackCny{{track-deployment}}:::ops
  rtTrackCny -.-> sentryDeployCny>Sentry deploy]

  dCny --> dPrd([gprd]):::ops
  dPrd -.-> |trigger| qaPrd2[/QA production/]:::ops
  dPrd -.-> |trigger| rtTrackPrd{{track-deployment}}:::ops
  rtTrackPrd -.-> sentryDeployPrd>Sentry deploy]
```

## Pipeline Improvements

The problems we have identified with the current architecture and what we intend to address with this feature are:

* release-tools pipelines occur on schedules and it is very difficult to follow the initial tagging, all the way through the production deploy. This can cause delays in noticing and debugging failures.
* A release-tools pipeline that creates a new Omnibus and/or CNG tag is indistinguishable from one that doesn't, making it difficult to find the origin of a tag, and thus find a pipeline.
* Deployer is triggered from omnibus-gitlab pipeline, but the Deployer pipeline also needs the image version to deploy to Kubernetes which comes from the CNG pipeline. This means we need to derive the CNG pipeline tag from the omnibus-gitlab tag, which is problematic for when there is an Omnibus-only or CNG-only change.
* There is no way to rollback a change without re-triggering the Deployer pipeline at the previous version, which would be a new pipeline to track in addition to the other pipelines created for auto-deploy.
* There are currently 3 projects that need to be manually deployed, by sending MRs for multiple version bumps. Delays to MR approval can cause delays to the process and being a manual process it is open to human error.
* There is no proper locking mechanism to prevent multiple concurrent deployments on the same environment.
* In general, deployment observability is very hard. Having a deployment dashboard that can show ongoing deployments by environment may help during daily operations.

## End state

Following the completion of this work, we will have a single coordinator for all deployment tasks. Having a single place to monitor deployment progress will help Release Managers to quickly identify and resolve deployment failures. Introducing additional deployment pipelines for satellite projects will also be simplified with them needing only to be an additional step initiated by the coordinator.

A single release-tools pipeline will:

1. Check for Omnibus and CNG changes
1. Commit those changes
1. Tag Omnibus and CNG to build packages
1. Wait for those packages to be available, then initiate a Deployer pipeline
1. Wait for the deployer to finish a stage, then initiate QA pipelines and track deployments

Having all of these operations in a single place will make it easier to follow the entire deployment process for a single deploy.

## Features that are not in scope

We have agreed that while the following items are important, they are out-of-scope for coordinated deployments:

* Increasing the speed of deploys. This includes improving MTTP, though we must at least maintain the current MTTP or better.
* Creating a coordinated deployment for the satellite projects. This work will be simplified by coordinated deployments but will not be considered a part of it. 
* Deploying from the master branch. Although we may start deploying from the master branch as part of coordinate deployments, this is covered separately by https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1209.
