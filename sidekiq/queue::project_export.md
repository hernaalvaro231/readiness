# Sidekiq Queue `project_export` Migration to Kubernetes

The decision to migrate one queue instead of all of sidekiq was made in the
following issue: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/590

`project_export` is a queue which allows users to export their projects from
GitLab.  An export consists of all Project data and repository data.  A full
listing of what is included in an export is here:
https://docs.gitlab.com/ee/user/project/settings/import_export.html#exported-contents

A bit of detail as to how the job works can be found inside of the GitLab
codebase: https://gitlab.com/gitlab-org/gitlab/blob/035e73598f8910b95b902b30dd3bcae7b3cd5d8d/lib/gitlab/import_export/shared.rb#L3-20

## Table of contents

  * [Architecture overview](#architecture-and-overview)
  * [Configuration](#configuration)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Resource Management](#resource-management)
  * [Security considerations](#security-considerations)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)
  * [Issue Awareness](#issue-awareness)
  * [Migration Path](#migration-path)

## Architecture and Overview

### Operational Diagram

```mermaid
sequenceDiagram
  participant user
  participant redis
  participant sidekiq
  participant project_export
  participant gitaly
  participant postgresql
  participant object storage

  user ->> redis: user requests export
  sidekiq ->> redis: monitoring queues
  sidekiq -> redis: plucks export job
  project_export ->> sidekiq: starts job

  activate project_export
    project_export ->> project_export: processing export job
    project_export ->> gitaly: export repository data
    project_export ->> postgresql: export project data
    project_export ->> object storage: export project content
    project_export ->> project_export: create tarball
    project_export ->> object storage: upload tarball
    project_export ->> redis: add email notification job
  deactivate project_export
```

## Configuration

The configuration of this queue is setup very similarly to our omnibus
configuration.  The defined role:
https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/dcf190eb49d16099f90418bdf67d450c81a71e1e/roles/gprd-base-be-sidekiq-export.json
The important items to note:

* we are using 4 worker queues per node
* each workers' concurrency limited to 1
* max memory allowable by the job is 16GB
* [We spread this across a fleet of 4
  servers](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/blob/56ff395edd8f2edeed141b0af459d8b0b8f0e693/environments/gprd/variables.tf#L434)
  * This provides a capacity of 16 total workers
* This queue is the _only_ queue processed on this fleet of servers

We will carry over this configuration into Kubernetes as well:
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/18a3efa89bce59bf2f3c6d28d37cdcd9ade7164f/values.yaml#L529-542

## Risk Assessment and Blast radius of failures

We utilize instance type `n1-highmem-8` with 8 CPU's and 52GB of RAM for this
fleet of servers, which is significantly different than the fleet of servers
powering our cluster.  GKE _currently_ uses `n1-standard-8` with 8 CPU's and
30GB of RAM.

With the autoscaling of both GKE nodes and HPA scale of these export Pods, we
should not see significant impact.  At most we should see Pods want to scale up
but due to memory limitations of the existing nodes, we'll need to wait for GKE
to spin up an additional node to add capacity to our cluster.

Due to the nature of how this service operates, we do not need multiple Pods
running by default.  We'll always have at least 1 running Pod, and as Kubernetes
deems necessary, we'll scale up to our HPA maximum configuration to limit the
amount of running jobs being processed by this queue.

Should this service fail, any requests to export a project will be left in the
queue until this service is back online.  Refer to [reliable
fetcher](./index.md#reliable-fetcher) for additional details.  See
[Testing](#testing) for further details.

Jobs that succeed appear to clean up after themselves.  It was recently
determined that failed jobs are not doing the same: https://gitlab.com/gitlab-org/gitlab/issues/196188

[Reference](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/606#note_265469866)

## Resource Management

Details into the `project_export` queue can be located here:
https://dashboards.gitlab.net/d/sidekiq-queue-detail/sidekiq-queue-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-queue=project_export

CPU Utilization of our VM's can be seen here: [VM CPU Utilization]

Memory Utilization of our VM's can be seen here: [VM Memory Utilization]

Disk Utilization of our VM's can be seen here: [Disk Utilization]

We appear to hover around 12% memory usage when the queue is idle.  This is
high, but consider that we run 4 workers per node and this usage counts all
processes on the system.  When observing an individual sidekiq worker process
behavior, each process appears to idle at roughly 700MB of used memory.  A
process that is actively performing work will spawn a few processes on the side
and this will be greatly impacted by the type of data to be exported that will
differ in both the content to export as well as how much data there is to
export.  On the surface it will not be simple to determine the maximum a process
may use.  Initial observations of workers processing data, we vary between 800MB
and 2GB of used memory for the sidekiq worker.

We are _currently_ relying the default HPA scale mechanism as defined in our
helm chart to scale up when the CPU of any Pod exceeds 350 millicores of CPU.
Until we see production-worthy traffic on this queue, it'll be hard to determine
what our scale needs.  At idle, empty queue, only 1 Pod runs.  This was observed
via testing in the `pre` environments.

We are _currently_ utilizing the default Resource Requests of 50 millicores CPU
and 650MB of RAM.  An idle process uses less than the CPU requested, and the RAM
usage appears to hover at 650MB in our Kubernetes environments, while hovering
around 700MB on our VM's.  More data is required to determine better values. See
[Testing](#testing) for details on work to gather more information to better
guage our resource requirements.

We set a limit on the RSS memory utilized by sidekiq to 16GB in both omnibus and
our Kubernetes configurations.  This is set as a sidekiq process killer for
omnibus, while this is a setting that is an upper limit for the Kubernetes Pods'
Deployment configuration.  When Kubernetes sees a Pod using more than 16GB of
RAM, the Pod will begin it's Termination sequence.

For CPU limits, an arbitrary value of 2 cores was chosen.

Data for the above can be found here: https://dashboards.gitlab.net/d/sidekiq-pod/sidekiq-pod-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=pre&var-cluster=pre-gitlab-gke&var-namespace=gitlab&var-Node=All&var-Deployment=gitlab-sidekiq-export

As mentioned earlier, this queue processes large amounts of data and will use a
temporary location to store files while performing work.  In order to limit the
amount of data is taken up on disk, we've created an EmptyDir mount to
`/srv/gitlab/shared` with a size limit of 50GB. This configurability is native
to our helm chart and is configured like shown here:
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/18a3efa89bce59bf2f3c6d28d37cdcd9ade7164f/values.yaml#L547-554
With this configuration, if a Pod were to exceed a total of 50GB of used data in
this directory, Kubernetes will Terminate the Pod.  _Note: There does not exist
a method to determine the amount of used disk space for this mount._  As noted
earlier, successful jobs should clean themselves up which should limit the
amount of data that accumulates over time.  Note that the EmptyDir configuration
uses the disk associated with the instance this Pod runs on.  If we see disk
usage across our fleet of servers grow too large, we'll be alerted and we'll
need to investigate a course of action.  Whether that be to lower the size limit
of this EmptyDir, or increase the disk size of our instances in GKE.

References:
* [Disk Cleanup](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/606#note_265469866)
* [Pod EmptyDir Limit Exceeding](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/merge_requests/105#note_260171299)

## Security Considerations

### Network Access

The Pods running Sidekiq will require the following:

  * Local inbound Prometheus metrics
  * Local outbound DNS
  * Local outbound Gitaly
  * Local outbound Postgresql
  * Local outbound Redis

Policy implementation: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/merge_requests/118

A Network Policy is not currently supported by our helm chart.
https://gitlab.com/gitlab-org/charts/gitlab/issues/1795

After the implementation of the above, we have an issue to create an appropriate
Network Policy: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/631

### Abuse

Our application should limit requests to prevent a high rate of requested
project exports from filling the queue.  Should abuse happen, we can follow our
existing documentation to identify and clear specific jobs waiting to be picked
up: https://docs.gitlab.com/ee/administration/troubleshooting/sidekiq.html#managing-sidekiq-queues

For identified security threats or situations where [blocking requests at the
haproxy frontends](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/block-things-in-haproxy.md)
does not suffice, we could stop processing any exports. We can quickly disable
the `project_export` queue by scaling down the number of Pods using the
Kubernetes API.

We need to document this capability still: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/636

## Observability and Monitoring

There are no alerts specific to the `project_export` queue.  We will rely on our
existing alerting configurations that are setup for sidekiq as a whole.  These
can be found here:

* https://gitlab.com/gitlab-com/runbooks/blob/master/rules/sidekiq-exceptions.yml
* https://gitlab.com/gitlab-com/runbooks/blob/master/rules/sidekiq-queues.yml
* https://gitlab.com/gitlab-com/runbooks/blob/master/rules/sidekiq-worker-apdex-alerts.yml

Our existing dashboards for Sidekiq have been supplemented with additional
dashboards and charts have been updated to ensure we incorporate details of our
Kubernetes deployments.  These dashboards can be found here:
https://dashboards.gitlab.net/dashboards/f/sidekiq/sidekiq-service

### Dashboards for `project_export` and Pods

* [Queue Detail](https://dashboards.gitlab.net/d/sidekiq-queue-detail/sidekiq-queue-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-queue=project_export)
* [Priority Detail](https://dashboards.gitlab.net/d/sidekiq-priority-detail/sidekiq-priority-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-priority=export)
* [Pod Info](https://dashboards.gitlab.net/d/sidekiq-pod/sidekiq-pod-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=pre&var-cluster=pre-gitlab-gke&var-namespace=gitlab&var-Node=All&var-Deployment=gitlab-sidekiq-export)
* [Application Info](https://dashboards.gitlab.net/d/sidekiq-app/sidekiq-application-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=pre&var-cluster=pre-gitlab-gke&var-namespace=gitlab&var-Deployment=gitlab-sidekiq-export)

## Responsibility

* Subject Matter Experts:
  * Kubernetes: [Infrastructure Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/)
  * `project_export`: [Imports Group](https://about.gitlab.com/handbook/engineering/development/)

## Testing

Testing to validate the usability of GitLab `project_export` queue inside of
Kubernetes was performed on the preproduction environment.

Validation of queue managed was performed in issue: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/630

Load testing this queue is performed in this issue: https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8908


### Logs

#### Successful Jobs

Sidekiq provides some basic logs as it picks up and processes work.  Here's an
example of a worker queue successfully pulling and completing work:

```
2020-01-15T15:17:25.329Z 8 TID-gnmzoaj6c ProjectExportWorker JID-5cec37a8b69e3191837dbe54 INFO: start
Saved Project export /srv/gitlab/shared/tmp/gitlab_exports/@hashed/0f/78/0f78540965a86402578f8188c826c1cb6c7ddcb608ae3a3201e532c7cacb6ce3/b87a06119f711912e88b7394b691e818/2020-01-15_15-17-503_jskarbek_www-gitlab-com_export.tar.gz
Import/Export - Project www-gitlab-com with ID: 399 successfully exported
[ActiveJob] Enqueued ActionMailer::DeliveryJob (Job ID: c06f2f26-f777-485c-bad0-03d2df1e8f8c) to Sidekiq(mailers) with arguments: "Notify", "project_was_exported_email", "deliver_now", #<GlobalID:0x00007fa26dcbde98 @uri=#<URI::GID gid://gitlab/User/12>>, #<GlobalID:0x00007fa26dcbd600 @uri=#<URI::GID gid://gitlab/Project/399>>
2020-01-15T15:19:03.457Z 8 TID-gnmzoaj6c ProjectExportWorker JID-5cec37a8b69e3191837dbe54 INFO: done: 98.128 sec
```

In the above we see the full process of the `project_export` worker.
1. We start a job with ID: `JID-5cec37a8b69e3191837dbe54`
1. We completed the export
1. We added a email to the queue
1. We closed down this worker thread after 98 seconds

#### Terminated Pods

As Pods are terminated, we send the appropriate signals to tell Sidekiq to stop
processing and requeue work.  An example of this is below:

```
2020-01-15T16:22:25.737Z 8 TID-gnmzoaj6c ProjectExportWorker JID-1c3b6993b74a6afcc25734c4 INFO: start
2020-01-15T16:22:38.967Z 8 TID-gnmwlbyqc INFO: Shutting down
2020-01-15T16:22:38.971Z 8 TID-gnmzoaiz4 INFO: Scheduler exiting...
2020-01-15T16:22:38.971Z 8 TID-gnmwlbyqc INFO: Terminating quiet workers
2020-01-15T16:22:38.971Z 8 TID-gnmzoajng INFO: Scheduler exiting...
2020-01-15T16:22:39.473Z 8 TID-gnmwlbyqc INFO: Pausing to allow workers to finish...
2020-01-15T16:22:43.537Z 8 TID-gnmwlbyqc WARN: Terminating 1 busy worker threads
2020-01-15T16:22:43.537Z 8 TID-gnmwlbyqc WARN: Work still in progress [#<struct Sidekiq::BaseReliableFetch::UnitOfWork queue="queue:project_export", job="{\"class\":\"ProjectExportWorker\",\"args\":[12,399,null,{}],\"retry\":3,\"queue\":\"project_export\",\"backtrace\":5,\"jid\":\"1c3b6993b74a6afcc25734c4\",\"created_at\":1579105345.7337694,\"correlation_id\":\"pSYDZSMdmF9\",\"enqueued_at\":1579105345.7357044}">]
2020-01-15T16:22:43.542Z 8 TID-gnmwlbyqc INFO: {:message=>"Pushed job 1c3b6993b74a6afcc25734c4 back to queue queue:project_export", :jid=>"1c3b6993b74a6afcc25734c4", :queue=>"queue:project_export"}
#<Thread:0x0000557975c54060@/srv/gitlab/lib/gitlab/popen.rb:41 run> terminated with exception (report_on_exception is true):
/srv/gitlab/lib/gitlab/popen.rb:41:in `read': stream closed in another thread (IOError)
        from /srv/gitlab/lib/gitlab/popen.rb:41:in `block (2 levels) in popen_with_detail'
2020-01-15T16:22:43.608Z 8 TID-gnmwlbyqc INFO: Bye!
#<Thread:0x0000557975c565e0@/srv/gitlab/lib/gitlab/popen.rb:42 run> terminated with exception (report_on_exception is true):
/srv/gitlab/lib/gitlab/popen.rb:42:in `read': stream closed in another thread (IOError)
        from /srv/gitlab/lib/gitlab/popen.rb:42:in `block (2 levels) in popen_with_detail'
```

In the above we see the following:
* 16:22:25 - work starts
* 16:22:38 - signal sent to shutdown
* 16:22:43 - sidekiq reqeues the job that did not complete

If Pods are terminated in an unclean manner, meaning the appropriate signal was
not sent to sidekiq, the [reliable fetcher] comes into play.  A job is left
dangling will eventually be picked up by a new Pod for processing.  This can be
seen in the following example logs:

```
2020-01-15T14:34:31.282Z 8 TID-gthv155us ProjectExportWorker JID-5cec37a8b69e3191837dbe54 INFO: start
```

Above we can see that job `JID-5cec37a8b69e3191837dbe54` started.  Then the Pod
that is processing this job is terminated, crashes, or perhaps the node
running the Pod disappears.  This job is left inside of Redis in the working
queue.  After some time has passed this is recycled and a new worker will pick
up the work to completion:

```
2020-01-15T15:17:25.329Z 8 TID-gnmzoaj6c ProjectExportWorker JID-5cec37a8b69e3191837dbe54 INFO: start
Saved Project export /srv/gitlab/shared/tmp/gitlab_exports/@hashed/0f/78/0f78540965a86402578f8188c826c1cb6c7ddcb608ae3a3201e532c7cacb6ce3/b87a06119f711912e88b7394b691e818/2020-01-15_15-17-503_jskarbek_www-gitlab-com_export.tar.gz
Import/Export - Project www-gitlab-com with ID: 399 successfully exported
[ActiveJob] Enqueued ActionMailer::DeliveryJob (Job ID: c06f2f26-f777-485c-bad0-03d2df1e8f8c) to Sidekiq(mailers) with arguments: "Notify", "project_was_exported_email", "deliver_now", #<GlobalID:0x00007fa26dcbde98 @uri=#<URI::GID gid://gitlab/User/12>>, #<GlobalID:0x00007fa26dcbd600 @uri=#<URI::GID gid://gitlab/Project/399>>
2020-01-15T15:19:03.457Z 8 TID-gnmzoaj6c ProjectExportWorker JID-5cec37a8b69e3191837dbe54 INFO: done: 98.128 sec
```

The work associated with testing this can be found in this issue: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/630

## Issue Awareness

The below contains tables that link all issues inside of this readiness review

#### The below ones are from this MR: 

| Issue Title                                                                               | Link                                                                                 | Blocker | Status |
|-------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|---------|--------|
| Clean the sidekiq Kubernetes configuration                                                | https://gitlab.com/gitlab-com/gl-infra/delivery/issues/591                           | Yes     | Closed |
| Configurations/Secrets SSOT between our omnibus installs and helm installs                | https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/167                            | Yes     | Closed |
| Enable auto-deploy for Kubernetes infrastructure                                          | https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/125                            | Yes     | Open   |
| How do we hot patch applications that are migrated into Kubernetes?                       | https://gitlab.com/gitlab-com/gl-infra/delivery/issues/641                           | ?       | Open   |
| The implementation of sidekiq does not allow for structured logging                       | https://gitlab.com/gitlab-org/charts/gitlab/issues/1757                              | No      | Open   |
| Sidekiq logs from GKE pods are entered into a new index                                   | https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8948                    | No      | Open   |
| project export uses temporary files that will build up over time when there are failures  | https://gitlab.com/gitlab-org/gitlab/issues/196188                                   | ?       | Open   |
| Adds networkpolicy for sidekiq                                                            | https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/118 | Yes     | Closed |
| Missing Network Policy configuration for Sidekiq                                          | https://gitlab.com/gitlab-org/charts/gitlab/issues/1795                              | Yes     | Closed |
| Test/Add working Network Policy to Sidekiq project_export                                 | https://gitlab.com/gitlab-com/gl-infra/delivery/issues/631                           | Yes     | Closed |
| Supplement our runbook with instructions on how to manually scale a deployment            | https://gitlab.com/gitlab-com/gl-infra/delivery/issues/636                           | No      | Closed |
| Test sidekiq job failure scenario                                                         | https://gitlab.com/gitlab-com/gl-infra/delivery/issues/630                           | Yes     | Closed |
| Loadtest sidekiq in kubernetes for project export                                         | https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8908                    | Yes     | Open   |


## Migration Path

The movement of this queue from VM's to Kubernetes can be test alongside each
other without a negative user experience.  If a job fails to be picked up by the
Pods, the job will simply remain on the queue until a VM instance picks up the
job.  If the job fails, it will be retried, potentially on a different server as
a failed job is requeued back into Redis for any instance to pick it up the next
retry attempt.  With this in mind, this will be the proposed method for
transitioning this queue into Kubernetes:

1. Stand up the production deployment
    * Limit the HPA configuration initially to only 1 Pod maximum
    * This will create a usable Pod (barring unforeseen problems)
1. Watch the logs coming from this Pod for successful "start" and "done"
   status's as the Pod processes exports
1. If successful, modify our scaling configuration to our desired configuration
   as noted above, maximum of 16 Pods
    * If not successful, revert and regroup
1. Turn down the number of VM's over the course of a week
    * Doing so slowly will allow us to evaluate the status of load on both our
      Pods as well as cluster scaling
1. Remove the nodes in terraform/chef/etc

[CNG]: https://gitlab.com/gitlab-org/build/CNG
[Disk Utilization]: https://thanos-query.ops.gitlab.net/graph?g0.range_input=1w&g0.max_source_resolution=0s&g0.expr=node_filesystem_size_bytes%7Benvironment%3D%22gprd%22%2C%20tier%3D%22sv%22%2C%20type%3D%22sidekiq%22%2C%20device%3D%22%2Fdev%2Fsda1%22%2C%20priority%3D%22export%22%7D%20%0A%20%20-%20%0Anode_filesystem_avail_bytes%7Benvironment%3D%22gprd%22%2C%20tier%3D%22sv%22%2C%20type%3D%22sidekiq%22%2C%20device%3D%22%2Fdev%2Fsda1%22%2C%20priority%3D%22export%22%7D&g0.tab=0
[VM CPU Utilization]: https://dashboards.gitlab.net/explore?orgId=1&left=%5B%22now-6h%22,%22now%22,%22Global%22,%7B%22expr%22:%22avg(instance:node_cpu_utilization:ratio%7Benvironment%3D%5C%22gprd%5C%22,%20type%3D%5C%22sidekiq%5C%22,%20stage%3D%5C%22main%5C%22,%20fqdn%3D~%5C%22sidekiq-export.*%5C%22%7D)%20by%20(fqdn)%5Cn%22,%22format%22:%22time_series%22,%22interval%22:%221m%22,%22intervalFactor%22:5,%22legendFormat%22:%22%7B%7B%20fqdn%20%7D%7D%22,%22datasource%22:%22Global%22,%22context%22:%22explore%22%7D,%7B%22mode%22:%22Metrics%22%7D,%7B%22ui%22:%5Btrue,true,true,%22none%22%5D%7D%5D
[VM Memory Utilization]: https://dashboards.gitlab.net/explore?orgId=1&left=%5B%22now-6h%22,%22now%22,%22Global%22,%7B%22expr%22:%22clamp_min(clamp_max(instance:node_memory_utilization:ratio%7Benvironment%3D%5C%22gprd%5C%22,%20type%3D%5C%22sidekiq%5C%22,%20stage%3D%5C%22main%5C%22,%20fqdn%3D~%5C%22sidekiq-export.*%5C%22%7D%5Cn,1),0)%5Cn%22,%22format%22:%22time_series%22,%22interval%22:%221m%22,%22intervalFactor%22:1,%22legendFormat%22:%22%7B%7B%20fqdn%20%7D%7D%22,%22datasource%22:%22Global%22,%22context%22:%22explore%22%7D,%7B%22mode%22:%22Metrics%22%7D,%7B%22ui%22:%5Btrue,true,true,%22none%22%5D%7D%5D
[k8s-workloads/gitlab-com]: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com
[mail_room]: https://github.com/tpitale/mail_room
[reliable fetcher]: https://gitlab.com/gitlab-org/Sidekiq-reliable-fetch/#gitlab-Sidekiq-fetcher
[Service Desk]: https://docs.gitlab.com/ee/user/project/service_desk.html
