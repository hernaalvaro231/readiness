# CI Artifacts CDN

## Summary

Currently when a runner attempts to download CI artifacts via the API,
the GitLab API will redirect the runner to a pre-signed URL pointing to
the CI artifacts bucket stored in Google Cloud Storage.

In order to save cost on network egress and to improve performance for
data transfer, we will be enabling this feature on GitLab.com, using a
[Google Cloud Load Balancer](https://cloud.google.com/load-balancing)
with [Cloud CDN](https://cloud.google.com/cdn/) enabled in front of our
existing Google Object Storage Bucket.

This readiness review is similar to the production readiness used in the [Container Registry CDN](../container-registry-cdn/index.md).

## Architecture

Once the API is configured to use the Cloud CDN, the request flow will
be altered so that clients will be redirected to
`https://cdn.artifacts.gitlab-static.net/<artifact path>` instead of
`https://storage.googleapis.com/<artifact path>` with a signed URL.

Note that the CDN will **only** be used if the request IP is outside the
[Google IP range](https://www.gstatic.com/ipranges/cloud.json).  There
is a [background Sidekiq job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/97709) that
fetches this URL and stores it in the Redis cache. This data is also
cached locally within the process to reduce load on Redis.

The following API endpoints will be affected:

|Typical caller|API endpoint|Sample # of hits in last 24 hours|Kibana link|
|--------------|------------|--------------------------|-----------|
|User          |GET `/api/v4/projects/:id/jobs/artifacts/:ref_name/download`|45,631|[details](https://log.gprd.gitlab.net/goto/ad22cb90-3e83-11ed-8d37-e9a2f393ea2a)|
|User          |GET `/api/v4/projects/:id/jobs/:job_id/artifacts`|666,504|[details](https://log.gprd.gitlab.net/goto/c12d4200-3e83-11ed-8d37-e9a2f393ea2a)|
|Runner        |GET `/api/v4/jobs/:id/jobs/artifacts`|2,495,206|[details](https://log.gprd.gitlab.net/goto/1f82e2b0-3e84-11ed-b0ec-930003e0679c)|

The Runner endpoint is called the most frequently because runners
automatically pass CI artifacts between stages.

[This merge request](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102839) adds
support for the UI endpoints, though clients use these endpoints
significantly less frequently. For example, for the GET
`Projects::ArtifactsController#download` case, there were [only 26,410
hits in the last 24
hours](https://log.gprd.gitlab.net/goto/5113f120-3e84-11ed-8d37-e9a2f393ea2a).

### Feature flag

Initially a [feature flag](https://gitlab.com/gitlab-org/gitlab/-/issues/373860)
will be available to direct certain projects and/or percentage of
requests to the CDN so this feature can be slowly rolled out on
production, once the configuration is enabled.

On GitLab.com, `proxy_download` is set to `false` for CI artifacts. By setting
this to `false`, clients may [directly download the file in object storage via a pre-signed URL](https://docs.gitlab.com/charts/charts/globals.html#consolidated-object-storage).

The Runner may disable this behavior via the `direct_download` parameter in the `GET /api/v4/jobs/:id/jobs/artifacts` endpoint. This is controlled via the [`FF_USE_DIRECT_DOWNLOAD` feature flag](https://docs.gitlab.com/runner/configuration/feature-flags.html). This feature flag has been made the [default since GitLab Runner v13.1.0](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2115),
but it can be disabled via environment variables.

If the runner fails to download an artifact for the first time, direct
download is switched off, and the CDN will not be used.

Note that `proxy_download` is set to `true` for most download types, as
seen in the [GitLab.com configuration](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/3f0ebd3ae7a7b87a59903216438def4fc786d010/releases/gitlab/values/values.yaml.gotmpl#L114-151). That
means these objects will only be retrieved from GitLab.com servers
directly. Object storage buckets with `proxy_download: true` will not incur Google Cloud Storage costs for network egress, but
the egress happens through the virtual machines and Cloudflare.

### Configuration

To enable the CDN, a `cdn` section under the `artifacts.object_store`
section in `gitlab.yml` will have to be added. [This merge request](https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests/2783)
added support to the GitLab Helm Chart. See the [documentation for configuring Google Cloud CDN with the Helm Chart](https://docs.gitlab.com/charts/advanced/external-object-storage/#google-cloud-cdn).

### Secrets

A new secret is needed for this feature, the CDN key that is used to generate signed URLs.
There is a unique key generated per environment (preprod, staging, production) and it is stored with other secrets in our GKMS vault.
Note that the [Container Registry CDN runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/cdn.md#secret-key-and-key-rotation) contains
details on how the secret for the Artifact CDN can be rotated in a similar fashion.

## Operational Risk Assessment

The Google CDN is Google managed infrastructure, with the existing
Google Storage bucket as a storage backend.  The
[`ci_job_artifacts_cdn`](https://gitlab.com/gitlab-org/gitlab/-/issues/373860)
feature flag will allow us to slowly roll this out on production and
monitor for any impact it has on CI artifacts usage.

Two services may be impacted:

1. API: Currently we already generate redirects to pre-signed URLs in
object storage. There is a little bit of CPU overhead in deciding
whether the IP is within the Google network (should be fast), but
generating a CDN-signed URL shouldn't require any more CPU than
generating a Google pre-signed URL.

2. Redis: There should be 1 additional request per process at startup,
and the process should retain this for 24 hours. We should not
expect to see much additional load from Redis.

## Security/Compliance

There will be additional logging collected by [the CDN stored in StackDriver](https://cloud.google.com/cdn/docs/logging), these logs are subject to the default StackDriver retention of 30days.
Cached Registry data will be distributed globally, instead of being served from the US region. This is similar to the existing CloudFlare CDN we use for web traffic, for a full list of locations see the [CloudCDN Points of presence](https://cloud.google.com/cdn/docs/locations).
There is an [issue opened with legal/compliance](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/740) relating to the Container Registry CDN.

## Cost savings

See [the analysis](https://gitlab.com/gitlab-org/gitlab/-/issues/360462)
and [example cost savings](https://gitlab.com/gitlab-org/gitlab/-/issues/370590#note_1087092161)
that suggests using Google CDN for CI artifacts will [significantly reduce costs](https://gitlab.com/gitlab-org/gitlab/-/issues/370590#note_1108078001).

## Performance

We expect performance to improve after enabling this feature since clients in regions outside of the US will be able to pull cached data instead of using the US regional storage bucket.
The performance improvement will be felt from the client pulling blobs from cache which is not something we can easily measure. Previous tests with the Container Registry
[can be found here](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/15004).

## Monitoring

If a CDN URL is used, the `json.meta.artifact_cdn_used` boolean will be `true` in the API logs in Kibana.

If a runner is unable to download the artifact for some reason, the build will fail with an error in the job log in the form:

```
ERROR: Downloading artifacts from coordinator... error couldn't execute GET against https://cdn.artifacts.gitlab-static.net
```

The Runner currently doesn't emit any special metrics when an artifact download has failed.

Stackdriver has metrics to record the cache hit and miss rate. We currently use this for [the container registry CDN metrics](https://gitlab.com/gitlab-com/runbooks/-/blob/5a98506042fb3e185de3ccf206945f6e8892b940/dashboards/registry/storage.dashboard.jsonnet#L130-177). See [the dashboards here](https://dashboards.gitlab.net/d/registry-storage/registry-storage-detail?from=now-6h%2Fm&to=now%2Fm&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&orgId=1).
