# Static assets hosting for Web IDE

## Summary

Some functionality of our VS Code-based Web IDE (principally the markdown preview feature) rely on assets that were
hosted by a third-party domain (vscode-cdn.net). For security reasons, we wanted to migrate from this third-party domain
to hosting these assets ourselves on GitLab controlled infrastructure.

Quoting from the [issue description](https://gitlab.com/gitlab-org/gitlab/-/issues/414488):

> The VS Code-based Web IDE uses service workers to sandbox the web views in the application. This security measure
> prevents malicious code in the web view from gaining access to the rest of the app, including your file system.
> However, to create these service workers, VS Code pulls data from an external server (vscode-cdn.net). A third party
> hosts this public domain and while it is not collecting any data from the GitLab instance, the HTTP header included
> in the request could be considered personal data.

Relevant issues:

- Request outlining the problem to solve: <https://gitlab.com/gitlab-org/gitlab/-/issues/414488>
- Reliability issue: <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/23877>

## Architecture

Any request for `*.cdn.web-ide.gitlab-static.net` or `*.staging.cdn.web-ide.gitlab-static.net` is processed the same
way:

1. Request hits Cloudflare
1. Cloudflare finds matching Worker route
1. Cloudflare fires up a Worker to process the request
1. Worker processes request by either fetching the content from cache/R2 or returning a response (e.g., 404)

See <https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitlab-static/web-ide-assets.md#traffic-routing> for
more info.

## Operational Risk Assessment

This service relies exclusively on Cloudflare features (e.g., workers, R2, etc).

## Security and Compliance

Unfortunately due to Cloudflare's lack of granularity on their access controls, it is not possible (currently) to limit
the scope of these API variables to the `gitlab-static.net` zone so they do have access to workers used in other zones.

These API tokens are stored in Vault and only provided to jobs that target the protected environments `staging` or
`production`. Only maintainers of the [gitlab-web-ide-vscode-fork repo](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork) have access to run jobs that target these environments.

We are not currently ingesting logs from Cloudflare Workers.

## Performance

Worker performance is not actively monitored or alerted on (not supported by Cloudflare). Metrics can be viewed at:

<https://dash.cloudflare.com/852e9d53d0f8adbd9205389356f2303d/workers/services/view/gitlab-web-ide-vscode-production/production>

## Monitoring and Alerts

Given that this is a service hosted entirely by Cloudflare features, we only have basic monitoring.

We have an uptime check configured in Pingdom, which targets the endpoint `/-/ping` and alerts the EOC if conditions are
met. This endpoint bypasses caching on the worker and fetches a dummy `ping` object from R2 as a form of end-to-end
test.

For information on the architecture and CI jobs involved in the deployment of Cloudflare Worker code & the VS Code
assets, please see the [runbooks page](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitlab-static/web-ide-assets.md).

## Responsibility

This service was built using Cloudflare Workers by the Reliability::General team.
