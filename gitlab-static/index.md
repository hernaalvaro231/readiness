# gitlab-static readiness reviews

gitlab-static.net is a zone hosted on Cloudflare with the following services:

- [web-ide-assets](./web-ide-assets.md) - assets used by our VS Code-based Web IDE
