
# Secure Files on GitLab.com

## Summary

**Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

Secure Files is a feature designed to support folks who need a simple way to add binary files to their CI pipelines. The primary use case is focused on the code signing and release process for mobile teams, but this approach has been generalized to support other use cases.

A recorded walkthrough of this feature can be seen at https://youtu.be/eK3FUskHfdo

**What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

To ensure a successful rollout, API logs and Sentry exceptions will be monitored for the following metrics:

1. [Sentry exceptions](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+secure_files)
2. [Total number of upload requests and response time for `POST /api/v4/projects/:project_id/secure_files`](https://dashboards.gitlab.net/d/api-rails-controller/api-rails-controller?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-controller=Grape&from=now-6h&to=now&var-action=POST%20%2Fapi%2Fprojects%2F:id%2Fsecure_files), and grouped by response code (2xx, 4xx, 5xx)
3. [Total number of download requests and response time for `GET /api/v4/projects/:project_id/secure_files/:secure_file_id/download`](https://dashboards.gitlab.net/d/api-rails-controller/api-rails-controller?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-controller=Grape&from=now-6h&to=now&var-action=GET%20%2Fapi%2Fprojects%2F:id%2Fsecure_files%2F:id%2Fdownload), and grouped by response code (2xx, 4xx, 5xx)
4. [Total number of delete requests and response time for `DELETE /api/v4/projects/:project_id/secure_files/:secure_file_id`](https://dashboards.gitlab.net/d/api-rails-controller/api-rails-controller?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-controller=Grape&from=now-6h&to=now&var-action=DELETE%20%2Fapi%2Fprojects%2F:id%2Fsecure_files%2F:id), and grouped by response code (2xx, 4xx, 5xx)


Business metrics to be focused on will be:

1. The number of files uploaded, which can be monitored in Sisense
2. Overall download requests, monitored via Prometheus metrics
3. Cost of storage in the GCS bucket

## Architecture

**Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.**

The following diagrams describe the primary actions for this feature:

## User uploads a file
```mermaid
sequenceDiagram
    participant User
    participant Rails
    participant DB
    participant Object Storage
    User->>Rails: POST /api/v4/projects/:project_id/secure_files
    Rails->>Object Storage: Rails encrypts the file and uploads it to Object Storage
    Object Storage->>Rails: Object Storage returns a reference to the file
    Rails->>DB: Rails sends the metadata for the file to the DB
    DB->>Rails: DB saves the metadata and returns the ID 
    Rails->>User: Rails returns the API response to the User
```

## User downloads a file
```mermaid
sequenceDiagram
    participant User
    participant Rails
    participant DB
    participant Object Storage
    User->>Rails: GET /api/v4/projects/:project_id/secure_files/:file_id/download
    Rails->>DB: Rails looks up the file for the given project_id and file_id
    DB->>Rails: DB returns the file metadata
    Rails->>Object Storage: Rails looks up the file from Object Storage based on the supplied metadata
    Object Storage->>Rails: Object Storage returns the encrypted file
    Rails->>User: Rails decrypts the file and returns it to the user
```

## User deletes a file
```mermaid
sequenceDiagram
    participant User
    participant Rails
    participant DB
    participant Object Storage
    User->>Rails: DELETE /api/v4/projects/:project_id/secure_files/:file_id
    Rails->>DB: Rails looks up the file for the given project_id and file_id
    DB->>Rails: DB returns the file metadata
    Rails->>Object Storage: Rails sends a request to delete the file from Object Storage
    Object Storage->>Rails: Object Storage returns a confirmation
    Rails->>DB: Rails sends a request to delete the entry from the DB
    DB->>Rails: DB returns a confirmation
    Rails->>User: Rails returns a confirmation to the user that the file was deleted
```

**Describe each component of the new feature and enumerate what it does to support customer use cases.**

The architecture of Secure Files consists of a table in the database, an encryption mechanism, and an object storage bucket.

1. The database table stores each file's metadata, such as file name, file checksum, and associated project.
2. The object storage bucket stores the encrypted contents of the file.
3. The [file uploader](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/uploaders/ci/secure_file_uploader.rb#L13) uses the randomly generated `key_data` value form the model to generate a unique encryption key per file. They encryption key is used by the Lockbox gem to encrypt the file using the AES-GCM algorithm.
4. Additional details about the upload and object storage structure were added to the docs in [this MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/80496)

This architecture is designed to create an easy-to-use API that enables users to securely upload and download files containing sensitive information and have that data stored within GitLab in a secure way.

**For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

If object storage were to become unavailable, the ability to upload, download, or delete files would result in a request failure with a 500 status and response of `500 Internal Server Error`. The ability to list files would not be impacted by an object storage outage.

If the database were to become unavailable, all API requests (upload, download, delete, and list) would result in a failure with a 500 status and response of `500 Internal Server Error`.

In both of these failure cases, CI pipelines using this feature would fail due to the necessary files being unavailable. The type of error message in the job logs would depend on how the user has set up their CI job.

**If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

There is a [hard limit of 100 files](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82858) that can be uploaded to a project. Any upload attempts over that limit will recieve a 400 response with the message `Maximum number of project ci secure files (100) exceeded`. API requests are limited by the application wide API rate limits. There are no known single points of failure in the design.

## Operational Risk Assessment

**What are the potential scalability or performance issues that may result with this change?**

Initially this change should pose little performance risk as it is a new feature and new API that will take time to get user adoption. 

A follow-up issue has been created to [Instrument encrypt/decrypt performance for Secure Files](https://gitlab.com/gitlab-org/gitlab/-/issues/360405) in order to better understand any potential operational risk associated with the encryption process.

**List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**

The main dependencies for this feature are the main Postgres database and Object Storage. If there is a failure in either of those dependencies, the feature will become unavailable, which would cause some CI jobs to fail for projects using this feature.

**Were there any features cut or compromises made to make the feature launch?**

No features were cut and no compromises were made in developing this feature.

**List the top three operational risks when this feature goes live.**

1. The file upload are processed by the rails app, and slow uploads could tie up puma workers.
2. File downloads are also processed by the rails app and could tie up puma workers as well.
3. Orphaned data could result from failures during the upload or download process https://gitlab.com/gitlab-org/gitlab/-/issues/357878
4. An abuse vector exists where an attacker could tie up Puma workers by sending very large files to the upload endpoint https://gitlab.com/gitlab-org/gitlab/-/issues/357998

**What are a few operational concerns that will not be present at launch, but may be a concern later?**

As the feature gets more user adoption in the future, heavy activity on the file upload or download APIs could introduce an operational concern.

**Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

A feature flag controls this feature rollout so that the feature can be disabled quickly in the case of performance issues. An Ops feature flag has also been created which adds the ability to put the API in a read-only mode in the event there is a need to limit the API interactions https://gitlab.com/gitlab-org/gitlab/-/merge_requests/84089

**Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

Users will interact with this feature primarily through the Secure Files API https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82330. 

**As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

A couple worst case scenarios that have been considered are:

1. Compromised encryption - since security is core to this feature any flaw in that security would create a problem. If the encryption key were to leak this would compromise the files stored. To address this a [per file encryption key](https://gitlab.com/gitlab-org/gitlab/-/issues/356607) has been implemented to minimize the risk. In the event of a key leak, only a single file would be affected.

1. Authorization failure - another risk would be that some change caused the authorization to fail in a way that files were able to be downloaded by unauthorized individuals. To address this risk the API authorization is tested against a [number of authorization scenarios](https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/requests/api/ci/secure_files_spec.rb).


## Database

**If we use a database, is the data structure verified and vetted by the database team?**

The data model was reviewed and approved in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77886.

**Do we have an approximate growth rate of the stored data (for capacity planning)?**

Since this feature is new, we don't have a sense of growth rate yet. However, a limit of 100 files per project has been put in place initially to limit potential abuse of the API https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82858.

**Can we age data and delete data of a certain age?**

This data should not be deleted as it is aged. Users are able to delete records [via the API](https://docs.gitlab.com/ee/api/secure_files.html#remove-secure-file), and data will be deleted when a [project is destoryed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82443).

## Security and Compliance

**Were the [gitlab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

Since this feature involves sensitive user data, security was the primary focus during feature development - an application security review was also performed as part of the development process https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/137. A couple of follow up issues were created as a result of the review. They will all be addressed before the feature rolls out:

1. https://gitlab.com/gitlab-org/gitlab/-/issues/355638
2. https://gitlab.com/gitlab-org/gitlab/-/issues/355653

**If this feature requires new infrastructure, will it be updated regularly with OS updates?**

No new infrastrucure requiring OS updates is needed for this feature.

**Has effort been made to obscure or elide sensitive customer data in logging?**

A follow-up task was created to mask sensitive data from Secure Files in CI job logs https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/44.

**Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?**

The sensitive file data for this feature is encrypted at rest.

**Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**


## Performance

**Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**

Performance tests were conducted using the custom [secure-files-performance](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/secure-files-performance) tool. The approach is described in the project [README](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/secure-files-performance/-/blob/master/README.md), but at a high level the tool will generate traffic to the Secure Files API by uploading and downloading a random set of files, and then kicking off CI pipelines that spawn many jobs which all download files. 

**Results**

Several tests were performed at various scales with the largest scale test being described below.

The [test job](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/secure-files-performance/-/jobs/2333873054) deleted, uploaded, and download 80 files per project across 10 projects using 5 different user accounts. The differnet user accounts were needed to get around rate limiting.

This test resulted in peak traffic of approximatly 30 request ops/s, 230 SQL ops/s, and 135 cache ops/s.

**Request Latency (mean)**

| Action   | Time  |
|----------|-------|
| Upload   | 405ms |
| Download | 191ms | 
| Delete   | 303ms |

![chart](./img/chart.png)

**Dashboards:**

1. [Rails Controller](https://dashboards.gitlab.net/d/api-rails-controller/api-rails-controller?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&var-controller=Grape&var-action=DELETE%20%2Fapi%2Fprojects%2F:id%2Fsecure_files%2F:secure_file_id&var-action=GET%20%2Fapi%2Fprojects%2F:id%2Fsecure_files&var-action=GET%20%2Fapi%2Fprojects%2F:id%2Fsecure_files%2F:secure_file_id%2Fdownload&var-action=POST%20%2Fapi%2Fprojects%2F:id%2Fsecure_files&from=1649889692861&to=1649890476112)
2. [Kube Containers Detail](https://dashboards.gitlab.net/d/api-kube-containers/api-kube-containers-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&from=1649889692861&to=1649890476112)
3. [Kube Deployment Detail](https://dashboards.gitlab.net/d/api-kube-deployments/api-kube-deployment-detail?orgId=1&from=1649889692861&to=1649890476112)
4. [API Overview](https://dashboards.gitlab.net/d/api-main/api-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&from=1649889692861&to=1649890476112)

**Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

No performance impacts on the database are expected due to enabling this feature. Still, the feature will be slowly rolled out to production via the feature flag while monitoring performance metrics.

**Are there any throttling limits imposed by this feature? If so how are they managed?**

This feature leverages existing GitLab API rate limits, and projects are limited to a maximum of 100 files, with a maximum file size of 5 megabytes.

**If there are throttling limits, what is the customer experience of hitting a limit?**

If a customer hits their file upload limit or the file size limit, the API request fails and returns the relevant error message.

**For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

No capabilities have been added to this specific feature to support retry and back-off strategies.

**Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

No capabilities have been added to this specific feature to address spikes in traffic.


## Backup and Restore

**Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**

**Are backups monitored?**

**Was a restore from backup tested?**

Backup and restore have not been implemented yet, but will be done in a follow-up task https://gitlab.com/gitlab-org/gitlab/-/issues/352874.

## Monitoring and Alerts

**Is the service logging in JSON format and are logs forwarded to logstash?**

Yes, this feature leverages the existing logging functionality in the rails application

**Is the service reporting metrics to Prometheus?**

Yes, this service leverages the existing metrics reporting in the rails application.

**How is the end-to-end customer experience measured?**

Not currently measured, however [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/360419) has been created to determine how this can be measured.

**Do we have a target SLA in place for this service?**

There is no target SLA in place at the moment, but [a follow-up issue](https://gitlab.com/gitlab-org/gitlab/-/issues/360426) has been added to track the work to getting this established as the feature rolls out.

**Do we know what the indicators (SLI) are that map to the target SLA?**

Not currently as there are no target SLA's in place.

**Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

No alerts are in place yet.

**Do we have troubleshooting runbooks linked to these alerts?**

No runbooks have been added as there are not alerts yet in place.

**What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

There are no customer notification thresholds in place yet.

**do the oncall rotations responsible for this service have access to this service?**

This feature leverages the existing monitoring and alerting in the GitLab rails application.

## Responsibility

**Which individuals are the subject matter experts and know the most about this feature?**

@darbyfrey is the SME for this feature.

**Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

@darbyfrey is the individual responsible for this feature.

**Is someone from the team who built the feature on call for the launch? If not, why not?**

Yes, @darbyfrey is the individual responsible for this feature and will be on call for the rollout.

## Testing

**Describe the load test plan used for this feature. What breaking points were validated?**

See the Performance section above with a description of the load testing performed.

**For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**

Three component failures were tested locally, database, object storage, and encryption.

1. Database failure - a database failure was simulated by disabling Postgres locally, which resulted in `500 Internal Server Error` errors for all requests.
2. Object storage failure - an object storage failure was simulated by misconfiguration the object storage bucket locally. This scenario resulted in `500 Internal Server Error` responses for the create, download, and delete endpoints. The list endpoint did not fail.
3. Decryption failure - a decryption failure was simulated by changing the encryption key of an existing file locally. This scenario resulted in `500 Internal Server Error` for the get and download endpoint. The list and delete endpoints did not fail.

**Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

Unit and request tests are run in CI covering the data model, file uploader, and API.
